import { Component, OnInit } from '@angular/core';
import { CardapioService } from './service/cardapio.service';
import { Abas, Itens } from './service/menu.model';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {

  mesa: number = 14;
  public idCardapio: number = 1;
  public cardapio: CardapioService;

  constructor(private cardapioService: CardapioService) { }

  ngOnInit(): void {
    this.cardapio = this.cardapioService;
    this.idCardapio = 1;
    this.callService();
  }

  cardapioChange(event) {
    console.log(event.detail.value[0]);
    this.idCardapio = event.detail.value[0];
    localStorage.setItem("idCardapio", event.detail.value[0]);

    this.callService();
  }

  callService() {
    localStorage.setItem('idCardapio', this.idCardapio + '');

    this.cardapioService.getHttpAbas(this.idCardapio).subscribe(resp => {
      this.cardapio.appAbas.abas = resp;
    });

    this.cardapioService.getHttpItens(this.idCardapio).subscribe(resp => {
      this.cardapio.appItens.itens = resp;
    });

    this.cardapioService.appPedidoItens = new Array;

  }

}
