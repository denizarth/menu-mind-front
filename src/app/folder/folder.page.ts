import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CardapioService } from '../service/cardapio.service';
import { Aba, Item, Itens } from '../service/menu.model';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  public idCardapioSecao: string;
  public title: string;
  public itensDisplay: Item[];
  public aba: Aba;

  constructor(private activatedRoute: ActivatedRoute, private cardapioService: CardapioService) { }

  ngOnInit() {
    const idCardapio = Number(localStorage.getItem('idCardapio'));
    this.idCardapioSecao = this.activatedRoute.snapshot.paramMap.get('id');
    this.title = this.activatedRoute.snapshot.paramMap.get('nome');

    if (this.idCardapioSecao === 'Completo') {
      this.itensDisplay = this.cardapioService.appItens.itens
      .filter(itemFiltro => idCardapio === itemFiltro.idCardapio);
      this.aba = {title: 'Completo', idCardapioSecao: 0, icon: ''};
    }
    else {
      this.itensDisplay = this.cardapioService.appItens.itens
      .filter(itemFiltro => this.idCardapioSecao === itemFiltro.idCardapioSecao + "");

      this.aba = this.cardapioService.appAbas.abas.find(aba => aba.idCardapioSecao == Number(this.idCardapioSecao));
    };

    console.log(this.aba)

  }

}
