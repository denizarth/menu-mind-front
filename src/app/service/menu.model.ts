export interface Abas {
  abas: Aba[];
}

export interface Aba {
  title: string;
  icon: string;
  idCardapioSecao: number;
}

export interface Itens {
  itens:Item[];
}

export interface Item {
  idCardapioItem?: number;
  idCardapio?: number;
  idCardapioSecao?: number;
  nome?: string;
  imagem?: string;
  descricao?: string;
  preco?: number;
  quantidade?: number;
  observacao?: string;
  sections?: Section[];
  itemAdicional?: string;
}

export interface Checkbox{
  val: string;
  isChecked: boolean;
}

export interface Section {
  nome: string;
  descricao?: string;
  limitePorItem?: number;
  quantidadeObrigatoria?: number;
  subItens:Item[]
}

export interface PedidoItem {
  idContaPedidoItem: number;
  idConta: number;
  idContaPedido: number;
  cardapioItem?: Item;
  idCardapioItemOpcional?: number;
  contaAdicional?: string;
  isReady: Boolean;

}
