import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Aba, Abas, Item, Itens, PedidoItem, Section } from './menu.model';

@Injectable({
  providedIn: 'root'
})
export class CardapioService {

  public appItens: Itens = { itens: [] };
  public appAbas: Abas = { abas: [] };
  public appPedidoItens: PedidoItem[];


  // API path
  base_path = 'http://menu-mind:8080';

  // Http Options
  httpOptions = {headers: new HttpHeaders({'Content-Type': 'application/json'})}

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };

  constructor(private http: HttpClient) { }

  // Get all data
  getHttpAbas(idCardapio:number): Observable<Aba[]> {
    return this.http.get<Aba[]>(this.base_path + '/cardapio/abas?idCardapio=' + idCardapio)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

  // Get all data
  getHttpItens(idCardapio: number): Observable<Item[]> {

    return this.http.get<Item[]>(this.base_path + '/cardapio/itens?idCardapio=' + idCardapio)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }
 /*
   // Get all data
  getHttpItemDetail(codigoItem: number): Observable<Item[]> {
    return this.http.get<Item[]>(this.base_path + '/itemdetail?codigo=' + codigoItem)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }



  // Get all data
  getHttpSubItens(): Observable<Section[]> {
    return this.http.get<Section[]>('http://localhost:3002' + '/sections')
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }*/


   // Get all data
   getHttpPedidoItem(idConta: number): Observable<PedidoItem[]> {
    return this.http.get<PedidoItem[]>(this.base_path + '/conta/itens?idConta=' + idConta)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

     // Get all data
     postHttpPedidoItem(pedidoItem: PedidoItem): Observable<PedidoItem> {
      return this.http.post<PedidoItem>(this.base_path + '/conta/item', pedidoItem)
      .pipe(
          retry(2),
          catchError(this.handleError)
        )
    }

}
