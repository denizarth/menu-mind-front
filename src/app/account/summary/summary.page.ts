import { Component, OnInit } from '@angular/core';
import { CardapioService } from 'src/app/service/cardapio.service';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.page.html',
  styleUrls: ['./summary.page.scss'],
})
export class SummaryPage implements OnInit {

  public cardapio: CardapioService;
  idConta: number = 1;

  constructor(private cardapioService: CardapioService) { }

  ngOnInit() {
    this.cardapio = this.cardapioService;
/*
    this.cardapioService.getHttpPedidoItem(this.idConta).subscribe(resp => {
      this.cardapio.appPedidoItens = resp;
    });*/

  }

}
