import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CardapioService } from '../service/cardapio.service';
import { Checkbox, Item, PedidoItem } from '../service/menu.model';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  public pedidoItem:PedidoItem = {idConta:0, idContaPedido:0 , idContaPedidoItem:0, isReady: false};

  public idCardapioItem: number;
  public appItemDetail: Item;
  public form;
  itemAdicionalSplit: Checkbox[];

  constructor(private activatedRoute: ActivatedRoute, private cardapioService: CardapioService) { }

  ngOnInit() {
    this.idCardapioItem = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.appItemDetail = this.cardapioService.appItens.itens.find(itemFiltro => this.idCardapioItem === itemFiltro.idCardapioItem);
    console.log(this.appItemDetail)

    if (this.appItemDetail.itemAdicional !== null) {
      // preenche itemAdicionalSplit
      this.itemAdicionalSplit = [];
      const adicionais = this.splitAdicional(this.appItemDetail.itemAdicional);
      console.log("adicionais: " + adicionais);
      for (const ad of adicionais) {
        this.itemAdicionalSplit.push({ val: ad, isChecked: false });
      }
    }

  }

  splitAdicional(theString: string) {
    return theString.split('|');
  }

  enviar(evento){
    console.log(evento);

    this.pedidoItem.idConta = 1;
    this.pedidoItem.cardapioItem = this.appItemDetail;
    this.pedidoItem.idContaPedido = 1;

    this.cardapioService.appPedidoItens.push(this.pedidoItem);

  }
}
